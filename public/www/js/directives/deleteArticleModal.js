/**
 * Created by Kevin on 15/12/2015.
 */

angular.module('deepNu.directives', []).directive('dnDeleteArticleModal', function() {
    return {
        restrict: "E",
        templateUrl: "views/elements/modal-delete-article.html",
        replace: true,
        link: function(scope, element) {
            console.log("dnDeleteArticleModal");

            scope.deleteArticle = function(id){
                console.log();
            }
        }
    }
});
