/**
 * Created by Kevin on 04/12/2015.
 */

angular.module('deepNu.directives').directive('dnToolbarSearch', function() {
    return {
        restrict: "E",
        templateUrl: "views/elements/toolbar-search.html",
        replace: true,
        link: function(scope, element) {
            console.log("dnToolbarSearch");
        }
    }
});
