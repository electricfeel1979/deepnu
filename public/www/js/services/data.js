/**
 * Created by Kevin on 21/12/2015.
 */

angular.module('deepNu.services', []).factory('data', function ($resource, $q) {
    var urlApi = "http://192.168.1.102:8000/api/";
    var resource = $resource(urlApi+':methodName/:modelName/:fieldName/:id/:val',
        {methodName: '@methodName', modelName: '@modelName', fieldName: '@fieldName', id: '@id'});

        //{charge: {method:'POST'}});

    function queryAllByField(methodName, modelName, fieldName, val) {
        return resource.query({methodName: methodName, modelName: modelName, fieldName: fieldName, val: val, id: '@id'});
    }

    function getAllSavedArticles (userid) {
        //var saveArticleResource = $resource(urlApi+'savedarticles/:userid', {userid: '@userid'});
        //return saveArticleResource.query({userid: userid});
        console.log('getAllSavedArticles');

        var saveArticleResource = $resource(urlApi+'getsavedarticles/:userid', {userid: '@userid'});
        return saveArticleResource.query({userid: userid});
    }

    return {
        /**
         * Get all documents
         * @param modelName - collection name
         * @returns {*|{method, isArray}|SchemaBuilderEntity} - array containing the query result
         */
        getAll: function (modelName) {
            return resource.query({modelName: modelName});
        },
        getArticles: function (userid) {
            var deferred = $q.defer();
            var articlesResource = $resource(urlApi+'getarticles');

            articlesResource.query().$promise.then(function (articles){
                getAllSavedArticles(userid).$promise.then(function (savedArticles) {
                    console.log(savedArticles);
                    _.forEach(savedArticles, function (article, key) {
                        console.log(1);
                        var index = _.findIndex(articles, {'_id': article.article_id});
                        articles[index].articleSaveId = article._id;
                        articles[index].isSaved = true;
                    });
                    deferred.resolve(articles);
                });
            });

            return deferred.promise;
        },
        /**
         * Get all saved articles
         * @param userid - userid of login user
         * @returns {*} - array of the saved articles of a user
         */
        getAllSavedArticles: getAllSavedArticles,
        /**
         * Save article that user saved
         * @param articleid - objectId
         */
        unsaveArticle: function(savearticleId) {
            return resource.delete({modelName: 'savedarticle', id: savearticleId})
        },
        saveArticle: function(articleid, userid) {
            //var deferred = $q.defer();
           var article = {article_id: articleid, user_id: userid};
            //console.log(article);
            //var savedArticleResource = $resource(urlApi+'savedarticle/');
            //
            //console.log(urlApi+'article/');
            //
            //articleResource.save(article,
            //    function(res) {
            //        deferred.resolve(res);
            //    },
            //    function(res) {
            //        deferred.reject(res);
            //    }
            //);

            return resource.save({modelName: 'savedarticle'}, article, function (data) {
                return data;
                //deferred.resolve(data);
            });

            //return deferred.promise;
        }
    }
});