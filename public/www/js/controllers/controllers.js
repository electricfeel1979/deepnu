/**
 * Created by Kevin on 30/11/2015.
 */

angular.module('deepNu.controllers', [])

.controller("SlidingMenuCtrl", function ($scope) {
    console.log("SlidingMenuCtrl");
})

.controller('PresidentialCandidatesCtrl', function ($scope, data) {
    console.log('PresidentialCandidatesCtrl');

    function dataOptions(height, showLegend, colors, legendPosition, margin) {
        return {
            chart: {
                type: 'pieChart',
                height: height,
                donut: true,
                showLabels: false,
                showLegend: showLegend,
                x: function(d){return d.name;},
                y: function(d){return d.score},
                duration: 500,
                legendPosition: legendPosition,
                color: colors,
                margin: margin
            }
        }
    }

    var socialMedianRankingOptionsColorFunction = function (d, i) {
        var key = i === undefined ? d : i;
        return d.color || color_scale(key);
    };

    $scope.options = dataOptions(150, false, null, null);
    $scope.socialMediaRankingOptions = dataOptions(200, false, null, "right");

    data.getAll('presidentcandidate').$promise.then(function (presCandidates){

        var presidentialCandidateBasic = {
            "Democratic": [],
            "Republican Party": []

        };

        $scope.candidates = [];

        _.forEach(presCandidates, function(n, key){

            $scope.candidates.push({
                "name": n.fname,
                "score": n.total_votes
            })

            if(n.type == "democratic"){
                presidentialCandidateBasic.Democratic.push(n)
            }else{
                var key = 'Republican Party';
                presidentialCandidateBasic[key].push(n)
            }
        });

        console.log(presidentialCandidateBasic);

        var totalVotes = _.sum(presidentialCandidateBasic, function(obj) {
            return _.sum(obj, function (obj2) {
                return obj2.total_votes;
            }) ;
        });

        console.log(totalVotes);

        $scope.presidentialCandidate = _.forEach(presidentialCandidateBasic, function(n, key){
            n.percentage = n.total;
            _.forEach(n, function(n2, k2) {
                n2.percentage = n2.total_votes;
                n2.total = [
                    {
                        "label": "totalVotes",
                        "score": n2.total_votes
                    },
                    {
                        "label": "totalVotesAllPresident",
                        "score": totalVotes
                    },
                ];
            });
        });
    });

    $scope.showCarousel = false;
    $scope.articles = [];

    var userid = '5677aaf52ac3052600630921';

    data.getArticles(userid).then(function (articles){
        console.log(articles);
        $scope.showCarousel = true;
        $scope.articles = articles;
        //$scope.activeMoreUrl = articles[0].url_link;
        //$scope.isArticleSaved = articles[0].isSaved;
        //$scope.activeArticle = articles[0]._id;
        $scope.activeArticle = articles[0];
    });

    $scope.saveArticle = function() {
        data.saveArticle($scope.activeArticle._id, userid).$promise.then(function (res){
            if(res._id){
                $scope.activeArticle.isSaved = true;
                $scope.activeArticle.articleSaveId = res._id;
            }
        });
    };

    $scope.unsaveArticle = function() {
        data.unsaveArticle($scope.activeArticle.articleSaveId).$promise.then(function (res){
            console.log(res);
            if(res.r) {
                $scope.activeArticle.isSaved = false;
            }
        });
    };

   $scope.topHeadlinesCarouselChange = function() {
       $scope.activeArticle = $scope.articles[topHeadlinesCarousel.getActiveCarouselItemIndex()];
      //$scope.activeMoreUrl = $scope.articles[topHeadlinesCarousel.getActiveCarouselItemIndex()].url_link;
       //$scope.isArticleSaved = $scope.articles[topHeadlinesCarousel.getActiveCarouselItemIndex()].isSaved;
       //$scope.activeArticle = $scope.articles[topHeadlinesCarousel.getActiveCarouselItemIndex()]._id;
   };

   $scope.readMore = function () {
       window.open($scope.activeArticle.url_link, '_system');
   };

   $scope.trendingHashtags = ["Immigration", "Taxes", "blah blah", "blah blah1", "blah blah2", "blah blah3", "blah blah4", "blah blah5"];

})

.controller('SearchHashtagsCtrl', function($scope) {
    console.log("SearchHashtagsCtrl");

    $scope.searchHashtagsContent = [
        {
            "title": "Congressional documents",
            "total": 2000
        },
        {
            "title": "News Articles",
            "total": 1000
        },
        {
            "title": "Social Comments",
            "total": 1000
        }
    ];

    $scope.openSelectBox = function() {
        console.log("openSelectBox");
    };


    $scope.nextSlide = function() {
        deepNewsCarousel.next();
    };

    $scope.prevSlide = function() {
        deepNewsCarousel.prev();
    };

})

.controller('ViewSavedArticlesCtrl', function($scope, data) {
    console.log('ViewSavedArticlesCtrl');

    var userid = '5677aaf52ac3052600630921';

    //$scope.savedArticles = false; //hide carousel first

    data.getAllSavedArticles(userid).$promise.then(function (savedArticles){
        //console.log(savedArticles);
        if(savedArticles){
            $scope.activeArticle = savedArticles[0];
            $scope.savedArticles = savedArticles;
            console.log(savedArticles);
        }
        //return savedArticles;
    });

    $scope.topHeadlinesCarouselChange = function() {
        var carouselActiveIndex = savedArticlesCarousel.getActiveCarouselItemIndex();
        $scope.activeMoreUrl = $scope.savedArticles[carouselActiveIndex]['article'].url_link;
    };

    $scope.readMore = function () {
        window.open($scope.activeArticle['article'].url_link, '_system');
    };

    $scope.deleteArticle = function(){
        data.unsaveArticle($scope.activeArticle._id).$promise.then(function (res){

            console.log(res);
            if(res.r) {
                var carouselActiveIndex = savedArticlesCarousel.getActiveCarouselItemIndex();
                //$scope.savedArticles = false;
                //delete $scope.savedArticles[carouselActiveIndex];
                _.pullAt($scope.savedArticles, carouselActiveIndex)
                //$scope.savedArticles =  $scope.savedArticles;
                $scope.activeMoreUrl = $scope.savedArticles[carouselActiveIndex]['article'].url_link;
                console.log( $scope.savedArticles);
                deleteArticleModal.hide();
                console.log(savedArticlesCarousel)
                //savedArticlesCarousel.refresh();
                if($scope.savedArticles.length == 0){
                    $scope.savedArticles = false;
                }
            }
        });
    };

    $scope.showDeleteArticleModal = function() {
        console.log("deleteArticle");
        deleteArticleModal.show();
    };
})
;