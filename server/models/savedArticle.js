/**
 * Created by Kevin on 21/12/2015.
 */

var mongoose = require('mongoose');

var savedArticleSchema = new mongoose.Schema({
    article_id: 'ObjectId',
    user_id: 'ObjectId',
    article_content: {
        $ref: 'String',
        $id: 'String'
    }
});

exports.name = 'savedarticle';
exports.Schema = mongoose.model('SavedArticle', savedArticleSchema);