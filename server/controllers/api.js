/**
 * Created by Kevin on 21/12/2015.
 */

var _ = require('lodash');

var ObjectId = require('mongoose').Types.ObjectId;

exports.install = function() {

    //F.restful('api/article/', [], json_api_query, json_api_get, json_api_save, json_api_delete);
    //F.restful('api/{model}/', [], json_api_query, json_api_get, json_api_save, json_api_delete);


    F.route('api/{model}/', json_api_query );
    F.route('api/{model}/{id}', json_api_get);
    F.route('api/{model}/', json_api_save, ['POST', 'OPTIONS']);
    F.route('api/{model}/{id}', json_api_delete, ['DELETE', 'OPTIONS', 'POST']);

    F.route('api/queryval/{model}/{field}/{val}/', [], json_api_query_field);
    F.route('api/getarticles/', [], getArticles);
    F.route('api/getsavedarticles/{userid}/', [], getSavedArticles);
};


function cors(self) {

    if (!self.cors('*', ['POST', 'GET', 'PUT', 'DELETE'])) {
        self.plain('NOT ALLOWED - OPTIONS - PREFLIGHT');
        //return;
    }

}


function getArticles() {

    console.log('getArticles');

    var self = this;

    cors(self);

    var Articles = MODEL('article').Schema;

    Articles.find('', '', {limit: 10}, function(err, articles) {
        self.json(articles);
    });

    //console.log('getArticles');
    //
    //var self = this;
    //
    //cors(self);
    //
    //var Article = MODEL('Article').Schema;
    //var SavedArticle = MODEL('savedarticle').Schema;
    //
    //var query = {'user_id': new ObjectId(userid)};
    //
    //Article.find('', '', {limit: 10}, function (err, articles) {
    //    //self.json(docs)
    //    SavedArticle.find(query, {article_id: 1, _id: 0}, function (err, userArticles) {
    //        var articlesWithUserLike = [];
    //    });
    //});
}

function getSavedArticles(userid){

    console.log('getSavedArticles');

    var self = this;

    cors(self);

    var SavedArticle = MODEL('savedarticle').Schema;
    var Article = MODEL('article').Schema;

    var query = {'user_id': new ObjectId(userid)};

    SavedArticle.find(query, 'article_id', function (err, docs) {

        var finalDoc = [];
        var articleIds = [];

        console.log(docs);

        _.forEach(docs, function(article, n){
            var articleToPush = {
                'article_id': article.article_id,
                '_id': article._id,
                'article': {}
            };
            console.log(articleToPush);
            finalDoc.push(articleToPush)
            articleIds.push(article.article_id)
        });

        console.log(articleIds);

        Article.find({'_id': {$in: articleIds}}, function (err, docs2) {
            console.log("hey hey hey");
            console.log(docs2);

            _.forEach(docs2, function(article2, n2){
                finalDoc[n2].article = article2;
            });

            console.log(finalDoc);
            self.json(finalDoc)
        });


    });
}

//function getSavedArticles(userid){
//
//    console.log('userid');
//
//    var self = this;
//
//    cors(self);
//
//    var SavedArticle = MODEL('savedarticle').Schema;
//    var Article = MODEL('article').Schema;
//
//    var query = {'user_id': new ObjectId(userid)};
//
//    SavedArticle.find(query, {article_id: 1, '_id': 1}, function (err, docs) {
//        console.log(docs);
//        var articles = [];
//        _.forEach(docs, function(n, key){
//            articles.push({'_id': new ObjectId(n.article_id)})
//        });
//        console.log(articles)
//        Article.find(articles, function (err, docs2) {
//            _.forEach(docs, function(n, key){
//
//                //n.article_content = docs2[key];
//            });
//            self.json(articles)
//        });
//    });
//}

function json_api_query_field(model, field, val) {

    console.log('json_api_query_field');

    var self = this;

    cors(self);

    var Model = MODEL(model).Schema;

    var query = {};
    query[field] = val;

    Model.find(query, function (err, docs) {
        self.json(docs);
    });

}

/*
 Description: Get
 Method: GET
 Output: JSON
 */
function json_api_query(model) {

    console.log(model);

    var self = this;

    cors(self);

    var Model = MODEL(model).Schema;

    console.log('query -> all');

    Model.find(function(err, docs) {
        self.json(docs);
    });
}

/*
 Description: Get
 Method: GET
 Output: JSON
 */
function json_api_get(model, id) {

    var self = this;

    cors(self);

    var Model = MODEL(model).Schema;

    console.log('get ->', id);

    Model.findById(id, function(err, doc) {
        self.json(doc);
    });

}

/*
 Description: Save
 Method: POST
 Output: JSON
 */
function json_api_save(model, id) {

    //console.log(model);
    console.log('json_api_save kevin');

    var self = this;

    //if(_.isEmpty(self.post))

    cors(self);

    var Model = MODEL(model).Schema;

    self.change(model+': save,  id: ' + id);
    //
    Model.findById(id, function(err, doc) {
        Model.create(self.post, function (err, doc) {
           //console.log(doc);
           self.json(doc);
        });
    });

    //return;

}

/*
 Description: Delete user
 Method: DELETE
 Output: JSON
 */
function json_api_delete(model, id) {

    var self = this;

    cors(self);

    // self.model('user').Schema;
    // framework.model('user').Schema;
    var Model = MODEL(model).Schema;

    console.log('delete ->', id);

    // What is it? https://github.com/totaljs/examples/tree/master/changes
    self.change(model +': deleted, id: ' + id);


    Model.findByIdAndRemove(id, function(err, doc) {
        if(err){
            self.json({r: false});
        } else {
            self.json({r: true});
        }
    });

    //Model.findById(id, function(err, doc) {
    //    // Please do not remove a document (THANKS :-))
    //    console.log(doc)
    //    doc.remove();
    //    self.json(doc);
    //});

}